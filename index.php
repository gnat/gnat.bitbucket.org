<?php 
$wrks = json_decode(file_get_contents("works.json"));
$wrks = $wrks->works;
$works = "";
$k = array();

foreach($wrks as $work):
  $k = array_merge($k, $work->k);
  $kw = implode(" ", $work->k);
  $works.='<div class="element '.$kw.'" data-href="'.$work->l.'" data-year="'.$work->y.'"><div class="content">';
  $works.='<div class="title">'.$work->t.'</div>';
  $works.='<div class="description">'.$work->d.'</div>';
  if(in_array("featured", $work->k)):
    $works.='<div class="is_featured"></div>';
  endif;
  $works.='</div></div>';
endforeach;

//keywords filter
$k = array_unique($k);
sort($k);
$filters = '<ul class="filter">';
$filters.='<li><a href="#">all</a></li>';
foreach($k as $v):
  $filters.='<li><a href="#.'.$v.'">'.$v.'</a></li>';
endforeach;
$filters.='</ul>';
?>
<html>
<head>
<title>gnat.in: portfolio</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
  <div id="wrapper">
    <div id="left">
      <div id="sitename">gnat.in</div>
      <div id="sort">
      <ul class="sort">
        <li><a href="#title">title</a></li>
        <li><a href="#year" class="chosen">year</a></li>
      </ul>
      </div>
      <div id="filters">
        <?php print $filters ?>
      </div>
      
      <div class="box-wrap antiscroll-wrap">
        <div class="box">
          <div class="antiscroll-inner">
            <div class="box-inner">
              <div class="works"><?php print $works; ?></div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="colophon">
        built using html + css + jquery
      </div>
    </div>
    <div id="right">
      <div class="cover"></div>
      <iframe src="" frameborder="0"></iframe>
    </div>
  </div>
</body>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="js/lib.js"></script>
<script type="text/javascript" src="js/script.js"></script>
</html>